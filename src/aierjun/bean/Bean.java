package aierjun.bean;

import javax.persistence.Entity;

@Entity
public class Bean {
	public String name;
	public String pass;
	public Integer identity;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Integer getIdentity() {
		return identity;
	}
	public void setIdentity(Integer identity) {
		this.identity = identity;
	}
	public Bean(String name, String pass, Integer identity) {
		super();
		this.name = name;
		this.pass = pass;
		this.identity = identity;
	}
	public Bean() {
		super();
	}
	
}
