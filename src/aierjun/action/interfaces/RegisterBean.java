package aierjun.action.interfaces;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import aierjun.bean.Bean;

@Controller
@RequestMapping("v1")
public class RegisterBean {
@RequestMapping( value="/login_post.json" , method= RequestMethod.POST)
@ResponseBody
public Object findOwner(@ModelAttribute Bean bean) {
	String name=bean.getName();
	String pass=bean.getPass();
	Integer identity=bean.getIdentity();
	if (name != null && pass != null && identity != null) {
			// 成功返回消息告诉Android端
			Map<String, String> map = new HashMap<String, String>();
			map.put("msg", "注册成功！");
			return map;
			} else {
			// 失败返回消息告诉Android端
			Map<String, String> map = new HashMap<String, String>();
			map.put("msg", "注册失败！");
			return map;
		}
	}
}
